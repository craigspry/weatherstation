/* 
 Based on HTU21D Humidity Sensor Library
 By: Nathan Seidle
 SparkFun Electronics
 Date: September 22nd, 2013
 License: This code is public domain but you buy me a beer if you use this and we meet someday (Beerware license).
 
 Get humidity and temperature from the HTU21D sensor.
 This has been modified to use the soft I2C libraray from https://code.google.com/p/rtoslibs/downloads/detail?name=DigitalIO20130221.zip
 
 */
 

#if defined(ARDUINO) && ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#include <DigitalIO.h>

#define HTDU21D_ADDRESS 0x80  //Unshifted 7-bit I2C address for the sensor

#define TRIGGER_TEMP_MEASURE_HOLD  0xE3
#define TRIGGER_HUMD_MEASURE_HOLD  0xE5
#define TRIGGER_TEMP_MEASURE_NOHOLD  0xF3
#define TRIGGER_HUMD_MEASURE_NOHOLD  0xF5
#define WRITE_USER_REG  0xE6
#define READ_USER_REG  0xE7
#define SOFT_RESET  0xFE

class HTU21DSoft {

public:
  HTU21DSoft();

  //Public Functions
  bool begin();
  int readHumidity(float&);
  int readTemperature(float&);
  void setResolution(byte resBits);

  //Public Variables

private:
  //Private Functions

  byte read_user_register(void);
  byte check_crc(uint16_t message_from_sensor, uint8_t check_value_from_sensor);

  //Private Variables
   SoftI2cMaster i2c;

};
